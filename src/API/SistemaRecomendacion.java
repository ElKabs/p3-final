package API;

import java.io.BufferedReader;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import VOS.VOUsuarioPelicula;

import com.csvreader.CsvReader;

import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOProgramacion;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import data_structures.Edge;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEncadenada;
import data_structures.ListaEnlazada;
import data_structures.ListaEnlazadaSimple;
import data_structures.MaxHeapCP;
import data_structures.Nodo;
import data_structures.NodoCamino;
import data_structures.WeightedGraph;
import VOS.VORating;
import VOS.VORecorrido;
import VOS.VORed;

public class SistemaRecomendacion implements ISistemaRecomendacion {
	VOTeatro[] arrayTeatros;
	VOPelicula[] arrayPeliculas;
	VOProgramacion[] arrayProgramacion;
	VORed[] arrayRed;
	String generos = "Adventure,Animation,Children,Comedy,Fantasy,Romance,Drama,Action,Crime,Thriller,Horror,Mystery,Sci-Fi,Documentary,IMAX,War,Musical,Western,Film-Noir,(no genres listed)";
	WeightedGraph< String, VOTeatro> grafoteatros = new WeightedGraph<>(101);
	EncadenamientoSeparadoTH<Integer, VOPelicula> hashPeliculas = new EncadenamientoSeparadoTH<>(97);
	EncadenamientoSeparadoTH<Integer, VOProgramacion[]> hashProgramacion = new EncadenamientoSeparadoTH<>(11);
	EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, Double>> hashSimilitudes = new EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, Double>>(101);
	EncadenamientoSeparadoTH< Long, VOUsuario> hashUsuarios = new EncadenamientoSeparadoTH<>(101);
	EncadenamientoSeparadoTH<String, VOGeneroPelicula> hashGeneros = new EncadenamientoSeparadoTH<>(13);
	EncadenamientoSeparadoTH<String, VOTeatro> hashTeatros = new EncadenamientoSeparadoTH<>(100);
	private Time ultimaFuncion;
	private WeightedGraph <VOTeatro, VOTeatro> grafoTeatros;
	private Time [][] horarios;
	public SistemaRecomendacion (){
		grafoTeatros =  new WeightedGraph <VOTeatro, VOTeatro> (100); 
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
		String [][] cambiosHorarios = {{"06:00","11:59"}, {"12:00","17:59"}, {"18:00", "23:59"}}; 
		horarios = new Time [3][2];
		for (int i = 0; i < horarios.length; i++)
		{
			for (int j = 0; j < horarios[i].length; j++)
			{
				try 
				{
					horarios[i][j]= new Time (formato.parse(cambiosHorarios[i][j]).getTime());
				} 
				catch (ParseException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}

	public int sizeTeatros() {
		return arrayTeatros.length;
	}

	public int sizeMovies() {
		return arrayPeliculas.length;
	}

	public boolean cargarPeliculas(String ruta)
	{
		try {
			Gson gson2 = new Gson();
			arrayPeliculas = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			arrayPeliculas = gson2.fromJson(br, VOPelicula[].class);
			br.close();
			for(VOPelicula peli : arrayPeliculas)
			{
				peli.parse();
				hashPeliculas.insertar(peli.getId(), peli);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public ISistemaRecomendacion crearSR()
	{
		return new SistemaRecomendacion();
	}
	//Requisito 1
	//El metodo carga las peliculas a partir de un archivo el cual entra por parametro
	//Lo guarda en hash de teatros
	public boolean cargarTeatros(String ruta)
	{
		try{
			Gson gson2 = new Gson();
			arrayTeatros = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			arrayTeatros = gson2.fromJson(br, VOTeatro[].class);
			br.close();
			for(VOTeatro teatro : arrayTeatros)
			{
				teatro.parse();
				grafoteatros.agregarVertice(teatro.getNombre(), teatro);
				hashTeatros.insertar(teatro.getNombre(), teatro);
			}

		}
		catch(Exception e )
		{
			e.printStackTrace();
			return false;
		}
		String[] stringGenerosArray = generos.split(",");
		for(String unString : stringGenerosArray)
		{
			VOGeneroPelicula newGenre = new VOGeneroPelicula();
			newGenre.setNombre(unString);
			hashGeneros.insertar(unString, newGenre);
		}
		return true;
	}
	//Requisito 2
	//Carga la cartelera de peliculas a partir de un archivo
	//Guarda la cartelera en hash de programacion
	public boolean cargarCartelera(String ruta) {
		try {
			Gson gson2 = new Gson();

			for (int i = 1; i < 6; i++) {

				String rutaJson = "./data/programacion/dia"+i+".json";

				arrayProgramacion = null;
				BufferedReader br = new BufferedReader(new FileReader(rutaJson));
				arrayProgramacion = gson2.fromJson(br, VOProgramacion[].class);
				hashProgramacion.insertar(i, arrayProgramacion);
			}
			completarInformacion();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	//Requisito 3
	//Este requisito carga la red a partir de un archivo
	//Guarda en la estructura en grafo teatros
	public boolean cargarRed(String ruta) {
		try {
			Gson gson2 = new Gson();
			arrayRed = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			arrayRed = gson2.fromJson(br, VORed[].class);
			br.close();

			for(VORed redArray : arrayRed)
			{
				redArray.parse();
				grafoteatros.agregarArco(redArray.teatroString, redArray.teatro2String, redArray.tiempoMin);
				grafoteatros.agregarArco(redArray.teatro2String, redArray.teatroString, redArray.tiempoMin);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	//Requisito 4
	//Crea un plan de peliculas a partir de un usuario por parametro y una fecha por parametro
	//La fecha es un int del 1 al 5
	//El metodo retorna un plan de peliculas a partir de los gustos de un usuario
	//Y teniendo en cuenta el tiempo de cada pelicula
	public ListaEnlazadaSimple<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha)  
	{
		ListaEnlazadaSimple<VOPeliculaPlan> listaRetorno = new ListaEnlazadaSimple<>();
		VOProgramacion[] peliculasPorDia = hashProgramacion.darValor(fecha);
		WeightedGraph<String, VOTeatro> franja1 = new WeightedGraph<>(13);
		WeightedGraph<String, VOTeatro> franja2 = new WeightedGraph<>(13);
		WeightedGraph<String, VOTeatro> franja3 = new WeightedGraph<>(13);
		try 
		{
			filtroPorfranja(fecha, 1);
			for(VOTeatro teatro1 : arrayTeatros)
				franja1.agregarVertice(teatro1.getNombre(), teatro1);

			filtroPorfranja(fecha, 2);
			for(VOTeatro teatro2 : arrayTeatros)
				franja2.agregarVertice(teatro2.getNombre(), teatro2);
			filtroPorfranja(fecha, 3);

			for(VOTeatro teatro3 : arrayTeatros)
				franja3.agregarVertice(teatro3.getNombre(), teatro3);

			for(VORed red1 : arrayRed)
			{
				franja1.agregarArco(red1.teatroString, red1.teatro2String, red1.tiempoMin);
			}
			for(VORed red2 : arrayRed)
			{
				franja2.agregarArco(red2.teatroString, red2.teatro2String, (red2.tiempoMin*1.15));
			}
			for(VORed red3 : arrayRed)
			{
				franja3.agregarArco(red3.teatroString, red3.teatro2String, (red3.tiempoMin*1.2));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return listaRetorno;
	}
	//Requisito 5
	//Este requisito otorga un plan de pelicula a partir de un usuario y de un genero por parametro
	//Retorna un plan de peliculas teniendo en cuenta tiempos 
	//Y retorna el plan con mas ocurrencias de un genero
	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario)
	{
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia1 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia2 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia3 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia4 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia5 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		dia1 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 1);
		dia2 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 2);
		dia3 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 3);
		dia4 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 4);
		dia5 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 5);
		ListaEnlazadaSimple<VOGeneroPelicula> generos1 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia1.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero1 = new VOGeneroPelicula();
			for(int j = 0; j<dia1.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero1.setNombre(dia1.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero1);
		}
		int ocurrencias1 = 0;
		for(int i = 0; i < generos1.darNumeroElementos(); i++)
		{
			if(generos1.darElemento(i).equals(genero))
			{
				ocurrencias1++;
			}
		}
		ListaEnlazadaSimple<VOGeneroPelicula> generos2 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia2.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero2 = new VOGeneroPelicula();
			for(int j = 0; j<dia2.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero2.setNombre(dia1.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero2);
		}
		int ocurrencias2 = 0;
		for(int i = 0; i < generos2.darNumeroElementos(); i++)
		{
			if(generos2.darElemento(i).equals(genero))
			{
				ocurrencias2++;
			}
		}
		ListaEnlazadaSimple<VOGeneroPelicula> generos3 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia3.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero3 = new VOGeneroPelicula();
			for(int j = 0; j<dia3.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero3.setNombre(dia3.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos3.agregarElementoFinal(genero3);
		}
		int ocurrencias3 = 0;
		for(int i = 0; i < generos3.darNumeroElementos(); i++)
		{
			if(generos3.darElemento(i).equals(genero))
			{
				ocurrencias3++;
			}
		}
		ListaEnlazadaSimple<VOGeneroPelicula> generos4 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia4.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero4 = new VOGeneroPelicula();
			for(int j = 0; j<dia4.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero4.setNombre(dia4.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos4.agregarElementoFinal(genero4);
		}
		int ocurrencias4 = 0;
		for(int i = 0; i < generos1.darNumeroElementos(); i++)
		{
			if(generos1.darElemento(i).equals(genero))
			{
				ocurrencias4++;
			}
		}
		ListaEnlazadaSimple<VOGeneroPelicula> generos5 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia5.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero5 = new VOGeneroPelicula();
			for(int j = 0; j<dia5.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero5.setNombre(dia5.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero5);
		}
		int ocurrencias5 = 0;
		for(int i = 0; i < generos5.darNumeroElementos(); i++)
		{
			if(generos5.darElemento(i).equals(genero))
			{
				ocurrencias5++;
			}
		}
		String masOcurrencias = "";
		int masOcurrente = ocurrencias5;
		ListaEncadenada<Integer> misOcurrencias = new ListaEncadenada<Integer>();
		misOcurrencias.agregarElementoFinal(ocurrencias1);
		misOcurrencias.agregarElementoFinal(ocurrencias2);
		misOcurrencias.agregarElementoFinal(ocurrencias3);
		misOcurrencias.agregarElementoFinal(ocurrencias4);
		misOcurrencias.agregarElementoFinal(ocurrencias5);
		for(int i = 0; i<misOcurrencias.size(); i++)
		{
			if(masOcurrente<misOcurrencias.darElemento(i))
			{
				masOcurrente=misOcurrencias.darElemento(i);
				masOcurrencias=""+i;
			}
		}
		if(masOcurrencias.equals("0"))
		{
			lista = dia1;
		}
		else if(masOcurrencias.equals("1"))
		{
			lista = dia2;
		}
		else if(masOcurrencias.equals("2"))
		{
			lista = dia3;
		}
		else if(masOcurrencias.equals("3"))
		{
			lista = dia4;
		}
		else if(masOcurrencias.equals("4"))
		{
			lista = dia5;
		}
		return lista;
	}
	
	//Requisito 6
	//Entra por parametro una franquicia, fecha. La franja puede o no existir
	//Retorna un plan de peliculas teniendo en cuenta solo una franquicia
	//Si entra una franja tiene en cuenta el tiempo de la pelicula y la hora
	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja)
	{
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOTeatro teatroActual =null;
		long horaActual = 0;
		long dia1 = 10L;
		long dia2 = 12L;
		long tarde1 = 12L;
		long tarde2 = 18L;
		if(franja.equals(""))
		{
			for(int i = 0; i<hashPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = hashPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						String teatro1 = teatroActual.getNombre();
						String teatro2 = teatroSiguiente.getNombre();
						double tiempoRecorrido = grafoteatros.darPesoArco(teatro1, teatro2);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
				}
			}
		}
		else if(franja.equals("mañana"))
		{
			for(int i = 0; i<hashPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = hashPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						String actual = teatroActual.getNombre();
						String siguiente = teatroSiguiente.getNombre();
						double tiempoRecorrido = grafoteatros.darPesoArco(actual, siguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
		}
		else if(franja.equals("tarde"))
		{
			for(int i = 0; i<hashPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = hashPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						String actual = teatroActual.getNombre();
						String siguiente = teatroSiguiente.getNombre();
						double tiempoRecorrido = grafoteatros.darPesoArco(actual, siguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
		}
		return lista;
	}

	//Requisito 7
	//Entra un genero por parametro y una fecha
	//Retorna un plan teniendo en cuenta el desplazamiento y un genero
	//Asi mismo, un int que indica una fecha del 1 al 5
	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) 
	{
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOPeliculaPlan mejorPlan = null;
		for (VOTeatro teatro : arrayTeatros)
		{
			VOPeliculaPlan mejorInterno = teatro.getPrimeraFuncion(fecha, genero.getNombre());
			if (mejorInterno == null) continue;
			if (mejorPlan == null || mejorInterno.getHoraInicio().getTime() < mejorPlan.getHoraInicio().getTime())
				mejorPlan = mejorInterno;
		}
		lista.agregarElementoFinal(mejorPlan);
		VORecorrido recorrerVo = new VORecorrido(mejorPlan.getHoraFin(), 0.0, lista, mejorPlan.getTeatro());
		VORecorrido voRecorridoMax = new VORecorrido(mejorPlan.getHoraFin(), 0.0, lista, mejorPlan.getTeatro());
		voRecorridoMax = buscarPlan (recorrerVo, voRecorridoMax, null, fecha, genero.getNombre(), null, 46, ultimaFuncion);
		return voRecorridoMax.listaFunciones;
	}

	//Requisito 8
	//Entra por parametro un genero, una fecha y una franquicia
	//Retorna un plan de peliculas teniendo en cuenta el genero
	//con mas ocurrencias y la misma franquicia
	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) 
	{
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOPeliculaPlan planFinal = null;
		for (VOTeatro teatro : arrayTeatros)
		{
			if (!teatro.getFranquicia().getNombre().equalsIgnoreCase(franquicia.getNombre()))
				continue;
			VOPeliculaPlan mejordeAdentro = teatro.getPrimeraFuncion(fecha, genero.getNombre());
			if (mejordeAdentro == null) continue;
			if (planFinal == null || mejordeAdentro.getHoraInicio().getTime() < planFinal.getHoraInicio().getTime())
				planFinal = mejordeAdentro;
		}
		lista.agregarElementoFinal(planFinal);

		VORecorrido vOrecorro = new VORecorrido(planFinal.getHoraFin(), 0.0, lista, planFinal.getTeatro());
		VORecorrido vorecorridoMax = new VORecorrido(planFinal.getHoraFin(), 0.0, lista, planFinal.getTeatro());
		vorecorridoMax = buscarPlan (vOrecorro, vorecorridoMax, null, fecha, genero.getNombre(), franquicia, 46, ultimaFuncion);
		return vorecorridoMax.listaFunciones;
	}

	//Requisito 9
	//Genera un mapa a partir del grafo de teatros
	//Para obtener un mapa se hace prim sobre el grafo de Teatros
	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() 
	{
		ListaEncadenada<IEdge<VOTeatro>> listaRetorno = new ListaEncadenada<>();
		Iterable <Edge<VOTeatro>> iteradorenEdge = grafoTeatros.prim();
		if (iteradorenEdge == null)
			return null;
		for (Edge<VOTeatro> edge : iteradorenEdge)
		{
			listaRetorno.agregarElementoFinal(edge);
		}
		return (ILista<IEdge<VOTeatro>>) listaRetorno;
	}

	//Requisito 10
	//Genera todas las rutas posibles del grafo de teatros
	//Se obtiene a traves de hacer DFS sobre todos los vertices
	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen1, int n)
	{
		EncadenamientoSeparadoTH<String, NodoCamino<String>> respuesta;
		ListaEnlazadaSimple respuesta1 = new ListaEnlazadaSimple();
		String origen = origen1.getNombre();
		respuesta = grafoteatros.DFS(origen);
		for (int i = 0; i < respuesta.darTamanio(); i++) 
		{
			int pasado = n;
			ListaEncadenada<VOTeatro> minirta = new ListaEncadenada<>();
			VOTeatro teatroActual = (VOTeatro) ((NodoCamino)respuesta.darValor(""+i)).darOrigen();
			String stringActual = teatroActual.getNombre();
			respuesta = grafoteatros.DFS(stringActual);
			for (int j = 0; j < respuesta.darCapacidad(); j++) 
			{
				if(pasado>=0)
				{
					minirta.agregarElementoFinal((VOTeatro) ((NodoCamino)respuesta.darValor(""+j)).darLlegada());
				}
				pasado--;
			}
			respuesta1.agregarElementoFinal(minirta);
		}
		return respuesta1;
	}

	//Metodos Auxiliares

	public double darMultiplicadorFranja (Time tiempo){
		long time = tiempo.getTime();
		if (time <= horarios[0][1].getTime())
			return 1.0;
		else if (time <= horarios[1][1].getTime())
			return 1.15;
		else
			return 1.2;
	}

	private VORecorrido buscarPlan(VORecorrido recorrido, VORecorrido max, VOUsuario usuario, int fecha, String genero, VOFranquicia franquicia, int numArcos, Time tiempoMax) {
		ListaEnlazadaSimple<Edge<VOTeatro>> lista = new ListaEnlazadaSimple<Edge<VOTeatro>>();
		for (Edge <VOTeatro> arcoTeatro : lista){
			VOTeatro teatroAlQueLlegue = arcoTeatro.other(recorrido.teatro);
			if (franquicia != null && !teatroAlQueLlegue.getFranquicia().getNombre().equalsIgnoreCase(franquicia.getNombre()))
				continue;
			Time tiempo = new Time(recorrido.tiempoActual.getTime() + ((int)(darMultiplicadorFranja(recorrido.tiempoActual)*arcoTeatro.weight()*60000)));
			if (tiempo.getTime() > tiempoMax.getTime())
			{
				if (max.compareTo(recorrido) <0)
					max = recorrido;
				continue;
			}
			VOPeliculaPlan ultimoPlanRecomendado = buscarPlanRecomendado (usuario, teatroAlQueLlegue, fecha, tiempo, recorrido, genero);
			if (ultimoPlanRecomendado == null)
			{
				if (max.compareTo(recorrido) <0)
					max = recorrido;
				continue;
			}
			VORecorrido nuevoRecorrido = null;
			if (usuario != null)
				nuevoRecorrido = recorrido.darNuevoRecorrido(usuario.darRecomendacion(ultimoPlanRecomendado.getPelicula()), ultimoPlanRecomendado, teatroAlQueLlegue);
			else
				nuevoRecorrido = recorrido.darNuevoRecorrido(0.0, ultimoPlanRecomendado, teatroAlQueLlegue);
			if (nuevoRecorrido.tiempoActual.getTime() > tiempoMax.getTime())
			{
				if (max.compareTo(nuevoRecorrido) <0)
					max = nuevoRecorrido;
				continue;
			}
			else
				max = buscarPlan(nuevoRecorrido, max, usuario, fecha, genero, franquicia, numArcos, tiempoMax);
		}
		return max;
	}

	private boolean cargarUsuarios()
	{
		String ruta = "./data/newRatings.csv";
		try(BufferedReader br = new BufferedReader(new FileReader(ruta))) 
		{
			String linea = br.readLine();
			while (linea!=null)
			{
				if (linea.startsWith("u"))
				{
					linea = br.readLine();
				}
				Long idusertemp, idbefore = 0L;
				String[] datos = linea.split(",");
				idusertemp = Long.parseLong(datos[0]);
				if( idbefore != idusertemp)
				{
					VOUsuario nuevo = new VOUsuario();
					nuevo.setId(idusertemp);
					hashUsuarios.insertar(idusertemp, nuevo);
					idbefore = idusertemp;
				}
				linea=br.readLine();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean cargarSimilitudes()
	{
		try {
			String rutaDeMatrizEnJson = "./data/simsMatriz.json";
			BufferedReader br = new BufferedReader(	new FileReader(rutaDeMatrizEnJson));
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(br);
			JsonArray obj = element.getAsJsonArray();
			for(JsonElement elemento : obj){

				EncadenamientoSeparadoTH<Integer, Double> hashActual= new EncadenamientoSeparadoTH(19);
				Integer idPeli =new Integer(0) ;
				Integer idPelitemp =new Integer(0) ;

				JsonObject obj2 =elemento.getAsJsonObject();
				Set<Map.Entry<String, JsonElement>> entries = obj2.entrySet();
				for (Map.Entry<String, JsonElement> entry: entries)
				{
					if(entry.getKey().equals("movieId"))
					{
						idPelitemp =Integer.parseInt(entry.getValue().getAsString());
						hashSimilitudes.insertar(idPelitemp, hashActual);
						break;
					}
					else
					{
						idPelitemp = Integer.parseInt(entry.getKey());
						Double sim;
						if (!entry.getValue().getAsString().equals("NaN")) sim =Double.parseDouble(entry.getValue().getAsString());
						else sim =0.0;
						hashActual.insertar(idPelitemp, sim);						 
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private VOPeliculaPlan buscarPlanRecomendado(VOUsuario usuario, VOTeatro llegada, int dia, Time tiempo, VORecorrido recorrido, String genero) 
	{
		VOPeliculaPlan recomendado = null;
		for (VOPeliculaPlan peliPlan : llegada.getFunciones(tiempo, dia))
		{
			if (recomendado!= null && peliPlan.getHoraInicio().getTime() - tiempo.getTime() > 3600*1000)
				break;
			if (usuario == null)
			{
				if ((genero == null || peliPlan.getPelicula().esGenero(genero)) && !recorrido.tienePelicula(peliPlan.getPelicula()))
					return peliPlan;
				else
					continue;
			}
			if ((genero == null || peliPlan.getPelicula().esGenero(genero)) && !recorrido.tienePelicula(peliPlan.getPelicula()))
			{
				if (recomendado == null || (usuario.compararPredicciones(recomendado.getPelicula(), peliPlan.getPelicula())<0))
					recomendado = peliPlan;
			}
		}
		return recomendado;
	}

	private void completarInformacion(){
		for (int i = 1; i < 6; i++) 
		{
			VOProgramacion[] programacionArr = hashProgramacion.darValor(i);
			for (int j = 0; j < programacionArr.length; j++) 
			{
				VOProgramacion a = programacionArr[j];
				for (int k = 0; k < arrayTeatros.length; k++)
					if(a.nombreTeatro.equals(arrayTeatros[k].getNombre()))
						arrayTeatros[k].getProgramacionDias().insertar(i, a);
			}
		}
	}

	private void filtroPorfranja(int fecha , int f) throws ParseException
	{
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm a");
		for(VOTeatro teatro1 : arrayTeatros){
			if(f==1)
			{
				VOProgramacion programa = teatro1.getProgramacionDias().darValor(fecha);
				for (int i = 0;  programa != null && i < programa.listapeliculas.movies.length; i++) 
				{
					for (int j = 0; j < programa.listapeliculas.movies[i].listaHoras.length; j++) 
					{
						String hora = programa.listapeliculas.movies[i].listaHoras[j].hora;
						hora = hora.replace('a', 'A');
						hora = hora.replace('p', 'P');
						hora = hora.substring(0,hora.indexOf('.'))+"M";
						if(hora.contains("P"))
							continue;
						else
						{
							Date horaa = formato.parse(hora);
							int horadeldia = horaa.getHours();
							if(!(horadeldia < 12) && !(horadeldia >= 8)){
								programa.listapeliculas.movies[i].listaHoras[j].hora = "00:00 PM";
							}
						}

					}
				}
			}
			if(f==2)
			{
				ListaEnlazadaSimple<VOTeatro> retorno= new ListaEnlazadaSimple<>();
				VOProgramacion pro = teatro1.getProgramacionDias().darValor(fecha);
				for (int i = 0; pro != null && i < pro.listapeliculas.movies.length; i++)
				{
					for (int j = 0; j < pro.listapeliculas.movies[i].listaHoras.length; j++) 
					{
						String hora = pro.listapeliculas.movies[i].listaHoras[j].hora;
						hora = hora.replace('a', 'A');
						hora = hora.replace('p', 'P');
						hora = hora.substring(0,hora.indexOf('.'))+"M";
						if(hora.contains("P")){
							continue;
						}else{
							Date horaa = formato.parse(hora);
							int horadeldia = horaa.getHours();
							if(!(horadeldia > 6) && !(horadeldia <= 12))
								pro.listapeliculas.movies[i].listaHoras[j].hora = "00:00 PM";
						}
					}	
				}

			}
			if(f==3)
			{
				ListaEnlazadaSimple<VOTeatro> retorno= new ListaEnlazadaSimple<>();
				VOProgramacion pro = teatro1.getProgramacionDias().darValor(fecha);
				for (int i = 0; pro != null && i < pro.listapeliculas.movies.length; i++) 
				{
					for (int j = 0; j < pro.listapeliculas.movies[i].listaHoras.length; j++) 
					{
						String hora = pro.listapeliculas.movies[i].listaHoras[j].hora;
						hora = hora.replace('a', 'A');
						hora = hora.replace('p', 'P');
						hora = hora.substring(0,hora.indexOf('.'))+"M";
						if(hora.contains("P")){
							continue;
						}
						else
						{
							Date horaa = formato.parse(hora);
							int horadeldia = horaa.getHours();
							if(!(horadeldia >= 6) && !(horadeldia < 12))
							{
								pro.listapeliculas.movies[i].listaHoras[j].hora = "00:00 PM";
							}
						}
					}	
				}

			}
			if(f>3)break;
		}

	}
}
