package VOS;

import java.util.regex.Pattern;

import data_structures.ListaEnlazadaSimple;
import data_structures.RedBlackBST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VOPelicula implements Comparable{
	
	@SerializedName("movie_id")
	@Expose
	private String idString;
	private int id;
	
	@SerializedName("title")
	@Expose
	private String titulo;
	
	@SerializedName("genres")
	@Expose
	private String generostring;
	private String[] generos;
	
	private RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> arbolFunciones [];
	
	public VOPelicula(int id, String titulo, String[] generos) {
	
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	
	public void parse(){
		if(idString != null && titulo != null && generostring != null){
			id = Integer.parseInt(idString);
			generos = generostring.split(Pattern.quote("|"));
			new VOPelicula(id, titulo, generos);
		}
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}


	public ListaEnlazadaSimple <VOPeliculaPlan> darPeliculasSiguienteFuncion(int dia, long siguiente)
	{
		ListaEnlazadaSimple <VOPeliculaPlan> funciones = new ListaEnlazadaSimple<VOPeliculaPlan>();
		
		for (ListaEnlazadaSimple <VOPeliculaPlan> planes : arbolFunciones[dia].inorden(true))
		{
			for (VOPeliculaPlan plan : planes)
			{
				if (plan.getHoraInicio().getTime() >= siguiente)
				{
					funciones.agregarElementoFinal(plan);
				}
			}
		}
		
		return funciones;
		
	}
	
	public boolean esGenero(String genero){
		for (String gen : generos)
			if (genero.equalsIgnoreCase(gen))
				return true;
		return false;
	}

}