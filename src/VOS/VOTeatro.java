package VOS;

import java.sql.Time;
import java.util.regex.Pattern;

import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEnlazadaSimple;
import data_structures.RedBlackBST;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro implements Comparable <VOTeatro> {
	
	EncadenamientoSeparadoTH<Integer, VOProgramacion> programacionDias = new EncadenamientoSeparadoTH<>(7);
	private RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> arbolFunciones [];
	
	
	@SerializedName("Nombre")
	@Expose
	private String nombre;
	
	@SerializedName("UbicacionGeografica(Lat|Long)")
	@Expose
	private String ubicacionString;
	
	public String getNombre() {
		return nombre;
	}

	public EncadenamientoSeparadoTH<Integer, VOProgramacion> getProgramacionDias() {
		return programacionDias;
	}

	public void setProgramacionDias(
			EncadenamientoSeparadoTH<Integer, VOProgramacion> programacionDias) {
		this.programacionDias = programacionDias;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacionString() {
		return ubicacionString;
	}

	public void setUbicacionString(String ubicacionString) {
		this.ubicacionString = ubicacionString;
	}

	public VOUbicacion getUbicacionR() {
		return ubicacionR;
	}

	public void setUbicacionR(VOUbicacion ubicacionR) {
		this.ubicacionR = ubicacionR;
	}

	public String getFranquiciaString() {
		return franquiciaString;
	}

	public void setFranquiciaString(String franquiciaString) {
		this.franquiciaString = franquiciaString;
	}

	public VOFranquicia getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(VOFranquicia franquicia) {
		this.franquicia = franquicia;
	}

	private VOUbicacion ubicacionR;
	
	@SerializedName("Franquicia")
	@Expose
	private String franquiciaString;
	
	private VOFranquicia franquicia;
	
	
	public VOTeatro() {
	// TODO Auto-generated constructor stub
	}

	public void parse(){
		if(ubicacionString != null && franquiciaString != null && nombre != null){
			String[] datosUbicacion = ubicacionString.split(Pattern.quote("|"));
			ubicacionR = new VOUbicacion();
			ubicacionR.setLatitud(Double.parseDouble(datosUbicacion[0]));
			ubicacionR.setLongitud(Double.parseDouble(datosUbicacion[1]));
			franquicia = new VOFranquicia();
			franquicia.setNombre(franquiciaString);
		}
	}
	public Iterable <VOPeliculaPlan> getFunciones (Time tiempoMin, int dia){
		ListaEnlazadaSimple <VOPeliculaPlan> lista = new ListaEnlazadaSimple<>();
		for (ListaEnlazadaSimple <VOPeliculaPlan> listaSimple: arbolFunciones[dia].darLlavesValoresEnRango(tiempoMin.getTime(), arbolFunciones[dia].max()))
			lista = lista.unirLista(listaSimple);
		return lista;
	}
	public VOPeliculaPlan getPrimeraFuncion (int dia){
		return arbolFunciones[dia].minValue().verPrimero();
	}
	public VOPeliculaPlan getPrimeraFuncion (int dia, String genero){
		for (ListaEnlazadaSimple<VOPeliculaPlan> listaPeli : arbolFunciones[dia]){
			for (VOPeliculaPlan peliPlan : listaPeli){
				if (peliPlan.getPelicula().esGenero(genero))
					return peliPlan;
			}
		}
		return null;
	}
	public VOPeliculaPlan getPrimeraFuncion (int dia, Time tiempo){
		for (ListaEnlazadaSimple<VOPeliculaPlan> listaPeli : arbolFunciones[dia]){
			for (VOPeliculaPlan peliPlan : listaPeli){
				if (peliPlan.getHoraInicio().getTime() >= tiempo.getTime())
					return peliPlan;
			}
		}
		return null;
	}
	@Override
	public int compareTo(VOTeatro arg0) {
		if (nombre.equals(arg0.nombre))
			return 0;
		return (nombre.compareTo(arg0.nombre) <0)? -1:1;
	}
}