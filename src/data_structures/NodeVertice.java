package data_structures;

import java.util.NoSuchElementException;

public class NodeVertice <K extends Comparable <K>,V>
{
	public K key;
	public V item;
	public RedBlackBST<Double, ListaEnlazadaSimple<Edge<K>>> edges;
	public boolean marcado;

	public NodeVertice (K key, V item){
		edges = new RedBlackBST<> ();
		this.item = item;
		this.key = key;
		this.marcado = false;
	}

	public int agregaEdge (Edge<K> edge){

		ListaEnlazadaSimple<Edge<K>> lista = null;
		try{
			lista = edges.get(edge.weight());
		}
		catch (NoSuchElementException e){
			lista = new ListaEnlazadaSimple<>();
			edges.put(edge.weight(), lista);
		}
		
		int n = lista.darNumeroElementos();
		lista.agregarElementoFinal(edge);
		return lista.darNumeroElementos() - n;
	}

	public ListaEnlazadaSimple <Edge<K>> adj (){
		ListaEnlazadaSimple <Edge<K>> lista = new ListaEnlazadaSimple <Edge<K>>();
		for (ListaEnlazadaSimple<Edge<K>> listaInterna : edges.inorden(true)){
			lista = lista.unirLista(listaInterna);
		}
		return lista;
	}

	public String toString (){
		return "K: " + key.toString() + "\tV: " + item.toString() + "\tGrado: " + edges.size();
	}
	public int grado(){
		return edges.size();
	}
}
